ssh-copy-id root@109.235.67.172

ssh root@109.235.67.172

# User
adduser --system --home /opt/renkam --ingroup www-data --disabled-login --gecos "" renkam

# Python
apt-get update -y
apt-get install -y \
    make \
    build-essential \
    libssl-dev \
    zlib1g-dev \
    libbz2-dev \
    libreadline-dev \
    libsqlite3-dev \
    wget \
    curl \
    llvm \
    libncurses5-dev \
    libncursesw5-dev \
    xz-utils \
    tk-dev \
    libffi-dev \
    liblzma-dev \
    python-openssl \
    git

git clone https://github.com/pyenv/pyenv.git /opt/python
export PYENV_ROOT=/opt/python
export PATH=$PYENV_ROOT/bin:$PATH
pyenv install 3.8.5

# Virtualenv
cd /opt/renkam
sudo -u renkam /opt/python/versions/3.8.5/bin/python -m venv env
sudo -u renkam env/bin/pip install -e "git+https://gitlab.com/sirex/rinkimai.git#egg=rinkimai"
sudo -u renkam env/bin/pip install -r env/src/rinkimai/requirements.txt

# PostgreSQL
apt-get install -y language-pack-lt
apt-get install -y postgresql postgresql-contrib
sudo -u postgres createuser renkam
sudo -u postgres createdb --owner renkam --template template0 --locale lt_LT.utf8 --encoding UTF8 renkam
sudo -u renkam psql

# Django
sudo -u renkam mkdir -p www/static
sudo -u renkam mkdir settings
sudo -u renkam mkdir import
sudo -u renkam tee settings/renkam_lt_settings.py > /dev/null <<EOF
from rinkimai.settings import *
from pathlib import Path
ALLOWED_HOSTS = ['renkam.lt', 'renkam.lt:8000']
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'renkam',
        'USER': 'renkam',
    }
}
STATIC_ROOT = '$PWD/www/static/'
IMPORT_DIR = Path('$PWD/import')
EOF
cat settings/renkam_lt_settings.py
sudo -u renkam tee env/lib/python3.8/site-packages/renkam.pth > /dev/null <<EOF
$PWD/settings
EOF
export DJANGO_SETTINGS_MODULE=renkam_lt_settings
sudo -u renkam --preserve-env env/bin/python env/src/rinkimai/manage.py migrate
sudo -u renkam --preserve-env env/bin/python env/src/rinkimai/manage.py collectstatic
sudo -u renkam --preserve-env env/bin/python env/src/rinkimai/manage.py createsuperuser
sudo -u renkam --preserve-env env/bin/python env/src/rinkimai/manage.py importcandidates 2016
sudo -u renkam --preserve-env env/bin/python env/src/rinkimai/manage.py runserver 0.0.0.0:8000

# Gunicorn
sudo -u renkam tee requirements.in > /dev/null <<EOF
gunicorn
EOF
sudo -u renkam env/bin/pip install pip-tools
sudo -u renkam --set-home env/bin/pip-compile
#| gunicorn==20.0.4          # via -r requirements.in
sudo -u renkam env/bin/pip install -r requirements.txt
sudo -u renkam env/bin/gunicorn -e DJANGO_SETTINGS_MODULE=renkam_lt_settings --bind 0.0.0.0:8000 rinkimai.wsgi
cat > /etc/systemd/system/gunicorn.service <<EOF
[Unit]
Description=gunicorn daemon
After=network.target

[Service]
User=renkam
Group=www-data
WorkingDirectory=$PWD
ExecStart=$PWD/env/bin/gunicorn -e DJANGO_SETTINGS_MODULE=renkam_lt_settings --workers 4 --bind unix:$PWD/run/gunicorn.sock rinkimai.wsgi

[Install]
WantedBy=multi-user.target
EOF
sudo -u renkam mkdir run
systemctl start gunicorn
systemctl enable gunicorn
systemctl status gunicorn
systemctl daemon-reload
systemctl restart gunicorn
journalctl -u gunicorn

# Nginx
apt-get install -y nginx
cat > /etc/nginx/sites-available/renkam <<EOF
server {
    listen 80;
    server_name renkam.lt;

    location = /favicon.ico { access_log off; log_not_found off; }
    location /static/ {
        alias $PWD/www/static/;
    }

    location / {
        include proxy_params;
        proxy_pass http://unix:$PWD/run/gunicorn.sock;
    }
}
EOF
cat /etc/nginx/sites-available/renkam
ln -s /etc/nginx/sites-available/renkam /etc/nginx/sites-enabled
systemctl restart nginx
systemctl status nginx
journalctl -u nginx

# Upgrade
ssh root@109.235.67.172
cd /opt/renkam
export DJANGO_SETTINGS_MODULE=renkam_lt_settings
sudo -u renkam git -C env/src/rinkimai pull --ff-only
sudo -u renkam env/bin/pip install -r env/src/rinkimai/requirements.txt
sudo -u renkam --preserve-env env/bin/python env/src/rinkimai/manage.py migrate
systemctl restart gunicorn
exit


# Troubleshooting

# 2020-07-24: requests.exceptions.HTTPError: 403 Client Error: Forbidden for url: https://www.vrk.lt

scp -r var/import root@109.235.67.172:/opt/renkam


# Other actions

# 2020-08-03 12:01 Add group slugs throug migrations


ssh root@109.235.67.172
cd /opt/renkam
export DJANGO_SETTINGS_MODULE=renkam_lt_settings
sudo -u renkam git -C env/src/rinkimai pull --ff-only
sudo -u renkam env/bin/pip install -r env/src/rinkimai/requirements.txt
sudo -u renkam --preserve-env env/bin/python env/src/rinkimai/manage.py showmigrations rinkimai
sudo -u renkam --preserve-env env/bin/python env/src/rinkimai/manage.py migrate
systemctl restart gunicorn
exit

sudo -u renkam --preserve-env env/bin/python env/src/rinkimai/manage.py shell <<EOF
from rinkimai.models import Group
for group in Group.objects.all():
    print(group.abbr, '|', group.name)
EOF
#| LŽA      | Lietuvos žaliųjų partija
#| LCA      | Lietuvos centro partija
#| DP       | Darbo partija
#| LVŽS     | Lietuvos valstiečių ir žaliųjų sąjunga
#| TS-LKD   | Tėvynės sąjunga - Lietuvos krikščionys demokratai
#| LSP      | Lietuvos socialdemokratų partija
#| LRLS     | Lietuvos Respublikos liberalų sąjūdis
#| LLRAŠS   | Lietuvos lenkų rinkimų akcija-Krikščioniškų šeimų sąjunga
#| PTIT     | Partija Tvarka ir teisingumas
#| ANPIKKK(CPLPP | Antikorupcinė N. Puteikio ir K. Krivicko koalicija (Lietuvos centro partija, Lietuvos pensininkų partiją)
#| LLP      | Lietuvos liaudies partija
#| PP„S     | Politinė partija „Lietuvos sąrašas“
#| SBITK„KIS(„LTS | S. Buškevičiaus ir Tautininkų koalicija „Prieš korupciją ir skurdą“ (Partija „Jaunoji Lietuva“, Tautininkų sąjunga)
#| „KPP     | „Drąsos kelias“ politinė partija
#| LLS(     | Lietuvos laisvės sąjunga (liberalai)


# 2020-08-03 12:12 Rerun group slug migration

sudo -u renkam --preserve-env env/bin/python env/src/rinkimai/manage.py showmigrations rinkimai
sudo -u renkam --preserve-env env/bin/python env/src/rinkimai/manage.py migrate --fake rinkimai 0007_group_slug
sudo -u renkam --preserve-env env/bin/python env/src/rinkimai/manage.py migrate rinkimai 0008_auto_20200724_1129
sudo -u renkam --preserve-env env/bin/python env/src/rinkimai/manage.py migrate --fake rinkimai 0011_auto_20200803_0841
sudo -u renkam --preserve-env env/bin/python env/src/rinkimai/manage.py migrate


# 2020-08-03 15:33 Add TLS certificate using Let's Encrypt

# https://letsencrypt.org/
ssh root@109.235.67.172
apt-cache search certbot
#| certbot - automatically configure HTTPS using Let's Encrypt
apt-get install certbot
#| Setting up certbot (0.27.0-1~ubuntu16.04.1) ...

# https://certbot.eff.org/
cat /etc/issue
#| Ubuntu 16.04.6 LTS \n \l
# certbot.eff.org documentation suggest a different way to install certbot.
# So I'm removing previously installed certbot:
apt-get purge certbot
# And installing in a certbot.eff.org recommended way:
apt-get update
apt-get install software-properties-common
add-apt-repository universe
add-apt-repository ppa:certbot/certbot
apt-get update
apt-get install certbot python3-certbot-nginx
certbot --nginx
#| You should test your configuration at:
#| https://www.ssllabs.com/ssltest/analyze.html?d=renkam.lt
#|
#|  - Congratulations! Your certificate and chain have been saved at:
#|    /etc/letsencrypt/live/renkam.lt/fullchain.pem
#|    Your key file has been saved at:
#|    /etc/letsencrypt/live/renkam.lt/privkey.pem
#|
#|  - Your account credentials have been saved in your Certbot
#|    configuration directory at /etc/letsencrypt. You should make a
#|    secure backup of this folder now. This configuration directory will
#|    also contain certificates and private keys obtained by Certbot so
#|    making regular backups of this folder is ideal.
#
# If Lithuanina center of Registers does not do backups, why should I? :D
certbot renew --dry-run
systemctl list-timers
#| NEXT                          LEFT       LAST                          PASSED       UNIT           ACTIVATES
#| Tue 2020-08-04 07:37:50 EEST  15h left   Mon 2020-08-03 15:37:11 EEST  21min ago    certbot.timer  certbot.service
systemctl status certbot.timer
#| ● certbot.timer - Run certbot twice daily
#|    Loaded: loaded (/lib/systemd/system/certbot.timer; enabled; vendor preset: enabled)
#|    Active: active (waiting) since Mon 2020-08-03 15:52:06 EEST; 8min ago
#| 
#| Aug 03 15:52:06 jg1e.c.dedikuoti.lt systemd[1]: Started Run certbot twice daily.
systemctl cat certbot.timer
#| # /lib/systemd/system/certbot.timer
#| [Unit]
#| Description=Run certbot twice daily
#| 
#| [Timer]
#| OnCalendar=*-*-* 00,12:00:00
#| RandomizedDelaySec=43200
#| Persistent=true
#| 
#| [Install]
#| WantedBy=timers.target
cat /etc/nginx/nginx.conf
cat /etc/nginx/sites-enabled/renkam
#| server {
#|     server_name renkam.lt;
#| 
#|     location = /favicon.ico { access_log off; log_not_found off; }
#|     location /static/ {
#|         alias /opt/renkam/www/static/;
#|     }
#| 
#|     location / {
#|         include proxy_params;
#|         proxy_pass http://unix:/opt/renkam/run/gunicorn.sock;
#|     }
#| 
#|     listen 443 ssl; # managed by Certbot
#|     ssl_certificate /etc/letsencrypt/live/renkam.lt/fullchain.pem; # managed by Certbot
#|     ssl_certificate_key /etc/letsencrypt/live/renkam.lt/privkey.pem; # managed by Certbot
#|     include /etc/letsencrypt/options-ssl-nginx.conf; # managed by Certbot
#|     ssl_dhparam /etc/letsencrypt/ssl-dhparams.pem; # managed by Certbot
#| 
#| }
#| server {
#|     if ($host = renkam.lt) {
#|         return 301 https://$host$request_uri;
#|     } # managed by Certbot
#| 
#| 
#|     listen 80;
#|     server_name renkam.lt;
#|     return 404; # managed by Certbot
#| 
#| 
#| }
# Done!
