import datetime
import uuid

import pytest
from django.contrib.auth.models import User

from rinkimai.forms import AddQuoteForm
from rinkimai.forms import AskQuoteSourceForm
from rinkimai.models import Quote
from rinkimai.sources import delfi
from rinkimai.sources import linkedin
from rinkimai.sources import lrsvoting
from rinkimai.sources import lrt
from rinkimai.sources import penkiolikamin
from rinkimai.testing.factories import GroupFactory
from rinkimai.testing.factories import PersonFactory
from rinkimai.testing.factories import QuoteFactory
from rinkimai.testing.factories import ReactionFactory
from rinkimai.services import get_current_election
from rinkimai.services import query_voter_candidates
from rinkimai.utils.dates import normalize_date
from rinkimai.utils.names import abbreviate
from rinkimai.utils.voter import Voter


@pytest.mark.django_db
def test_candidate_rating():
    q1 = QuoteFactory(text='Q1', person__name='C1')
    ReactionFactory(quote=q1, private_key='R1', reaction=1)
    ReactionFactory(quote=q1, private_key='R2', reaction=1)
    ReactionFactory(quote=q1, private_key='R3', reaction=-1)

    q2 = QuoteFactory(text='Q2', person__name='C1')
    ReactionFactory(quote=q2, private_key='R1', reaction=1)
    ReactionFactory(quote=q2, private_key='R2', reaction=-1)
    ReactionFactory(quote=q2, private_key='R3', reaction=1)

    q3 = QuoteFactory(text='Q3', person__name='C2')
    ReactionFactory(quote=q3, private_key='R1', reaction=-1)
    ReactionFactory(quote=q3, private_key='R2', reaction=1)
    ReactionFactory(quote=q3, private_key='R3', reaction=-1)

    q4 = QuoteFactory(text='Q4', person__name='C1')
    ReactionFactory(quote=q4, private_key='R1', reaction=-1)

    voter = Voter()
    voter.set_key_pair('R1')
    election = get_current_election()
    assert query_voter_candidates(election, voter) == [
        {
            'slug': 'c1',
            'name': 'C1',
            'rating': 33,
            'party': {
                'slug': 'dp',
                'abbr': 'DP',
                'name': 'Darbo partija',
            },
        },
        {
            'slug': 'c2',
            'name': 'C2',
            'rating': -100,
            'party': {
                'slug': 'dp',
                'abbr': 'DP',
                'name': 'Darbo partija',
            },
        },
    ]


@pytest.mark.vcr
@pytest.mark.django_db
def test_delfi_lt():
    PersonFactory(name='Aurelijus Veryga')
    PersonFactory(name='Saulius Skvernelis')
    PersonFactory(name='Gitanas Nausėda')
    assert delfi.parse('https://www.delfi.lt/news/daily/lithuania/veryga-gales-liepti-nesioti-kaukes-ir-ivesti-privaloma-izoliacija-atvykstantiems.d?id=84780173') == {
        'date': datetime.date(2020, 7, 16),
        'names': [
            'Aurelijus Veryga',
            'Saulius Skvernelis',
            'aurelijus veryga',
        ]
    }


@pytest.mark.vcr
@pytest.mark.django_db
def test_delfi_lt_2():
    assert delfi.parse('https://www.delfi.lt/news/daily/lithuania/vrk-vadove-siems-rinkimams-idiegti-internetini-balsavima-gali-pritrukti-laiko.d?id=84728503') == {
        'date': datetime.date(2020, 7, 9),
        'names': []
    }


@pytest.mark.vcr
@pytest.mark.django_db
def test_15min_lt():
    PersonFactory(name='Aurelijus Veryga')
    PersonFactory(name='Saulius Skvernelis')
    PersonFactory(name='Gitanas Nausėda')
    assert penkiolikamin.parse('https://www.15min.lt/verslas/naujiena/energetika/i-mitinga-netoli-prezidenturos-atejes-g-nauseda-apsikabino-v-landsbergi-siuose-rumuose-nedarysime-nieko-kas-butu-nenaudinga-lietuvai-664-1348372') == {
        'date': datetime.date(2020, 7, 16),
        'names': ['Gitanas Nausėda'],
    }


@pytest.mark.vcr
@pytest.mark.django_db
def test_15min_lt_2():
    PersonFactory(name='Aurelijus Veryga')
    PersonFactory(name='Saulius Skvernelis')
    PersonFactory(name='Gitanas Nausėda')
    assert penkiolikamin.parse('https://www.15min.lt/naujiena/aktualu/lietuva/teismas-nusprende-kad-vyriausybe-pazeide-zurnalistu-teises-istrindama-pasitarimo-irasa-56-1351676') == {
        'date': datetime.date(2020, 7, 23),
        'names': ['Saulius Skvernelis'],
    }


@pytest.mark.vcr
@pytest.mark.django_db
def test_lrt_lt():
    PersonFactory(name='Nerijus Vitkauskas')
    assert lrt.parse('https://www.lrt.lt/naujienos/pozicija/679/1200674/nerijus-vitkauskas-valstybei-reikia-hakeriu') == {
        'date': datetime.date(2020, 7, 26),
        'names': ['Nerijus Vitkauskas'],
    }


@pytest.mark.vcr
@pytest.mark.django_db
def test_lrt_lt():
    PersonFactory(name='Rūta Miliūtė')
    assert lrt.parse('https://www.lrt.lt/naujienos/lietuvoje/2/1201901/linkevicius-po-seimo-rinkimu-ir-toliau-noretu-darbuotis-uzsienio-politikos-srityje') == {
        'date': datetime.date(2020, 7, 29),
        'names': ['Rūta Miliūtė'],
    }


@pytest.mark.vcr
def test_lrs_voting_lt():
    result = lrsvoting.parse('https://www.lrs.lt/sip/portal.show?p_r=37067&p_bals_id=-39287')
    reactions = result.pop('reactions')

    assert result == {
        'date': datetime.date(2020, 6, 30),
        'names': [],
        'quote': '',
    }

    assert reactions[0] == {
        'date': None,
        'name': 'Vida Ačienė',
        'quote': 'Už',
        'reaction': 1,
    }


@pytest.mark.skip("LinkedIn gives 404 response for requests lib.")
@pytest.mark.vcr
def test_linkedin_comment():
    result = linkedin.parse('https://www.linkedin.com/feed/update/urn:li:article:8997185398901410626?commentUrn=urn%3Ali%3Acomment%3A%28article%3A8997185398901410626%2C6234286828028067840%29')
    assert result == {}


@pytest.mark.django_db
def test_voter_keys(client):
    resp = client.get('/')
    assert resp.status_code == 200
    assert uuid.UUID(resp.context['voter'].private_key).version == 4
    assert uuid.UUID(resp.context['voter'].public_key).version == 4


def _show_quotes(quotes):
    return [
        {
            'person': q['person']['name'],
            'text': q['text'],
        }
        if q['person'] else
        {
            'group': q['group']['name'],
            'text': q['text'],
        }
        for q in quotes
    ]


def _show_quote_reactions(reactions):
    return [
        {
            'person': r['person']['name'],
            'text': r['text'],
            'reaction': r['reaction'],
        }
        for r in reactions
    ]


@pytest.mark.django_db
def test_quote_list(client):
    QuoteFactory(person__name='C1', text='Q1')
    QuoteFactory(person__name='C1', text='Q2')
    QuoteFactory(person__name='C2', text='Q3')
    QuoteFactory(person__name='C2', text='Q4', active=False)
    resp = client.get('/')
    assert resp.status_code == 200
    assert _show_quotes(resp.context['quotes']) == [
        {'person': 'C2', 'text': 'Q3'},
        {'person': 'C1', 'text': 'Q2'},
        {'person': 'C1', 'text': 'Q1'},
    ]


@pytest.mark.django_db
def test_save_reaction(client):
    quote = QuoteFactory(person__name='C1', text='Q1')
    resp = client.post('/save-reaction', data={
        'quote': quote.pk,
        'reaction': 1,
    })
    assert resp.status_code == 200
    assert resp.json() == {
        'errors': None
    }

    resp = client.get('/reactions')
    assert _show_quote_reactions(resp.context['quotes']) == [
        {'person': 'C1', 'text': 'Q1', 'reaction': 1},
    ]


@pytest.mark.django_db
def test_update_reaction(client):
    quote = QuoteFactory(person__name='C1', text='Q1')

    # Create new reaction
    resp = client.post('/save-reaction', data={
        'quote': quote.pk,
        'reaction': 1,
    })
    assert resp.status_code == 200
    assert resp.json() == {
        'errors': None
    }

    resp = client.get('/reactions')
    assert _show_quote_reactions(resp.context['quotes']) == [
        {'person': 'C1', 'text': 'Q1', 'reaction': 1},
    ]

    # Update previously saved reaction
    resp = client.post('/save-reaction', data={
        'quote': quote.pk,
        'reaction': -1,
    })
    assert resp.status_code == 200
    assert resp.json() == {
        'errors': None
    }

    resp = client.get('/reactions')
    assert _show_quote_reactions(resp.context['quotes']) == [
        {'person': 'C1', 'text': 'Q1', 'reaction': -1},
    ]


@pytest.mark.vcr
@pytest.mark.django_db
def test_add_quote(client):
    source = 'https://www.delfi.lt/news/daily/lithuania/veryga-gales-liepti-nesioti-kaukes-ir-ivesti-privaloma-izoliacija-atvykstantiems.d?id=84780173'

    saulius = PersonFactory(name='Saulius Skvernelis')

    # Ask for source URL
    resp = client.get('/add-quote')
    assert resp.status_code == 200
    assert isinstance(resp.context['form'], AskQuoteSourceForm)

    # Prefilled quote form from given source
    resp = client.get('/add-quote', {
        'source': source,
    })
    assert resp.status_code == 200
    assert isinstance(resp.context['form'], AddQuoteForm)
    assert resp.context['form'].initial['date'] == datetime.date(2020, 7, 16)
    assert resp.context['names'] == [
        {
            'id': str(saulius.pk),
            'label': 'Saulius Skvernelis',
            'type': 'person',
        },
    ]

    # Add new quote
    resp = client.post('/add-quote', {
        'source': source,
        'suggested_author': f'person:{saulius.pk}',
        'person': '',
        'date': '2020-07-16',
        'text': 'Q1',
        'tags': 'T1, T2',
    })
    assert resp.status_code == 302, (
        resp.context['form'].errors
        if resp.context and 'form' in resp.context else
        resp.status_code
    )
    quote = Quote.objects.get(text='Q1')
    assert resp.url == f'/quote/{quote.pk}'
    assert quote.person == saulius


@pytest.mark.django_db
def test_person_quote_details(client):
    QuoteFactory(person__name='C1', group=None, text='Q1')
    quote = QuoteFactory(person__name='C2', group=None, text='Q2')
    resp = client.get(f'/quote/{quote.pk}')
    assert resp.status_code == 200
    assert _show_quotes(resp.context['quotes']) == [
        {'person': 'C2', 'text': 'Q2'},
    ]


@pytest.mark.django_db
def test_group_quote_details(client):
    QuoteFactory(person=None, group__name='G1', text='Q1')
    quote = QuoteFactory(person=None, group__name='G2', text='Q2')
    resp = client.get(f'/quote/{quote.pk}')
    assert resp.status_code == 200
    assert _show_quotes(resp.context['quotes']) == [
        {'group': 'G2', 'text': 'Q2'},
    ]


@pytest.mark.vcr
@pytest.mark.django_db
def test_add_quote_and_person(client):
    source = 'https://www.delfi.lt/news/daily/lithuania/veryga-gales-liepti-nesioti-kaukes-ir-ivesti-privaloma-izoliacija-atvykstantiems.d?id=84780173'

    # Add new quote without person
    resp = client.post('/add-quote', {
        'source': source,
        'suggested_author': 'person',
        'person': 'Aurelijus Veryga',
        'date': 'January 6, 2017',
        'text': 'Q1',
        'tags': 'T1, T2',
    })
    assert resp.status_code == 302, (
        resp.context['form'].errors
        if resp.context and 'form' in resp.context else
        resp.status_code
    )
    quote = Quote.objects.get(text='Q1')
    assert resp.url == f'/add-person?quote={quote.pk}&person=Aurelijus+Veryga'

    # Redirect to add person form
    resp = client.get(resp.url)
    assert resp.status_code == 200

    # Submit data about a new person
    resp = client.post(f'/add-person?quote={quote.pk}', {
        'name': "Aurelijus Veryga",
    })
    assert resp.status_code == 302
    assert resp.url == f'/quote/{quote.pk}'

    quote = Quote.objects.get(pk=quote.pk)
    assert quote.person.name == 'Aurelijus Veryga'


@pytest.mark.django_db
def test_person_quotes(client):
    QuoteFactory(person__name='C1', text='Q1')
    QuoteFactory(person__name='C2', text='Q2')
    QuoteFactory(person__name='C1', text='Q3')
    QuoteFactory(person__name='C2', text='Q4', active=False)
    resp = client.get(f'/person/c1')
    assert resp.status_code == 200
    assert _show_quotes(resp.context['quotes']) == [
        {'person': 'C1', 'text': 'Q3'},
        {'person': 'C1', 'text': 'Q1'},
    ]


@pytest.mark.django_db
def test_group_quotes(client):
    g1 = GroupFactory(name='G1')
    g2 = GroupFactory(name='G2')
    QuoteFactory(person=None, group=g1, text='Q1')
    QuoteFactory(person=None, group=g2, text='Q2')
    QuoteFactory(person=None, group=g1, text='Q3')
    QuoteFactory(person=None, group=g1, text='Q4', active=False)
    resp = client.get(f'/group/g1')
    assert resp.status_code == 200
    assert _show_quotes(resp.context['quotes']) == [
        {'group': 'G1', 'text': 'Q3'},
        {'group': 'G1', 'text': 'Q1'},
    ]


@pytest.mark.vcr
@pytest.mark.django_db
def test_add_group_quote(client):
    source = 'https://tslkd2020.lt/programa/slubuojanti-demokratija-ir-chaotiskas-viesasis-valdymas/#sparciau-atversime'

    ts_lkd = GroupFactory(
        name="Tėvynės sąjunga - Lietuvos krikščionys demokratai",
        abbr="TS-LKD",
        aliases=["Konservatoriai"],
    )

    resp = client.get('/add-quote', {'source': source})
    assert resp.status_code == 200
    assert resp.context['names'] == [
        {
            'id': str(ts_lkd.pk),
            'label': 'Tėvynės sąjunga - Lietuvos krikščionys demokratai',
            'type': 'group',
        },
    ]

    # Add new quote without person
    resp = client.post('/add-quote', {
        'source': source,
        'suggested_author': 'group',
        'group': ts_lkd.pk,
        'date': '2020-06-22',
        'text': 'Q1',
        'tags': 'T1, T2',
    })
    quote = Quote.objects.get(text='Q1')
    assert quote.group == ts_lkd
    assert quote.person is None
    assert resp.status_code == 302
    assert resp.url == f'/quote/{quote.pk}'


@pytest.mark.vcr
@pytest.mark.django_db
def test_approve_quote(client):
    user = User.objects.create_user(
        'test',
        'test@example.com',
        'secret',
        is_staff=True,
    )
    client.login(username='test', password='secret')
    quote = QuoteFactory(person=None, group__name='G1', text='Q1', active=False)
    resp = client.post('/approve-quote', {
        'quote': quote.pk,
        'active': True,
    })
    assert resp.status_code == 200
    quote = Quote.objects.get(text='Q1')
    assert quote.active is True
    assert quote.approved_by == user


@pytest.mark.vcr
@pytest.mark.django_db
def test_approve_quote_unauthorized(client):
    quote = QuoteFactory(person=None, group__name='G1', text='Q1', active=False)
    resp = client.post('/approve-quote', {
        'quote': quote.pk,
    })
    assert resp.status_code == 302
    assert resp.url == '/'
    quote = Quote.objects.get(text='Q1')
    assert quote.active is False
    assert quote.approved_by is None


@pytest.mark.vcr
@pytest.mark.django_db
def test_disapprove_quote(client):
    User.objects.create_user(
        'test',
        'test@example.com',
        'secret',
        is_staff=True,
    )
    client.login(username='test', password='secret')
    quote = QuoteFactory(person=None, group__name='G1', text='Q1')
    resp = client.post('/approve-quote', {
        'quote': quote.pk,
        'active': False,
    })
    assert resp.status_code == 200
    quote = Quote.objects.get(text='Q1')
    assert quote.active is False
    assert quote.approved_by is None


@pytest.mark.django_db
def test_unapproved_quotes(client):
    User.objects.create_user(
        'test',
        'test@example.com',
        'secret',
        is_staff=True,
    )
    client.login(username='test', password='secret')
    QuoteFactory(person=None, group__name='G1', text='Q1')
    QuoteFactory(person=None, group__name='G2', text='Q2')
    QuoteFactory(person=None, group__name='G1', text='Q3', active=False)
    QuoteFactory(person=None, group__name='G1', text='Q4', active=False)
    resp = client.get('/unapproved-quotes')
    assert resp.status_code == 200
    assert _show_quotes(resp.context['quotes']) == [
        {'group': 'G1', 'text': 'Q4'},
        {'group': 'G1', 'text': 'Q3'},
    ]



@pytest.mark.parametrize('abbr, name', [
    ('LŽP', 'Lietuvos žaliųjų partija'),
    ('LCP', 'Lietuvos centro partija'),
    ('DP', 'Darbo partija'),
    ('LVŽS', 'Lietuvos valstiečių ir žaliųjų sąjunga'),
    ('TS-LKD', 'Tėvynės sąjunga - Lietuvos krikščionys demokratai'),
    ('LSP', 'Lietuvos socialdemokratų partija'),
    ('LRLS', 'Lietuvos Respublikos liberalų sąjūdis'),
    ('LLRA-KŠS', 'Lietuvos lenkų rinkimų akcija-Krikščioniškų šeimų sąjunga'),
    ('PTT', 'Partija Tvarka ir teisingumas'),
    ('APKK', 'Antikorupcinė N. Puteikio ir K. Krivicko koalicija (Lietuvos centro partija, Lietuvos pensininkų partiją)'),
    ('LLP', 'Lietuvos liaudies partija'),
    ('LS', 'Politinė partija „Lietuvos sąrašas“'),
    ('BTK-PKS', 'S. Buškevičiaus ir Tautininkų koalicija „Prieš korupciją ir skurdą“ (Partija „Jaunoji Lietuva“, Tautininkų sąjunga)'),
    ('DK', '„Drąsos kelias“ politinė partija'),
    ('LLS', 'Lietuvos laisvės sąjunga (liberalai)'),
])
def test_abbreviate(abbr, name):
    assert abbreviate(name) == abbr


@pytest.mark.parametrize('user, norm', [
    ('January 6, 2017', '2017-01-06'),
])
def test_normalize_date(user, norm):
    assert normalize_date(user).isoformat() == norm
