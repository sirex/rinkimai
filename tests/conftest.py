import pytest


@pytest.fixture(scope='session')
def vcr_config():
    return {
        'record_mode': 'new_episodes',
    }
