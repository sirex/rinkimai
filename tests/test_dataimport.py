import pytest
from django.core.management import call_command

from rinkimai.models import Candidate


@pytest.mark.vcr
@pytest.mark.django_db
def test_2016_parliament():
    call_command('importcandidates', '2016', limit=10)
    call_command('importcandidates', '2016', limit=10)
    assert list(
        Candidate.objects.
        filter(election__date__year=2016).
        values_list(
            'number',
            'kind',
            'person__name',
            'group__name',
        )
    ) == [
        (11, 'daugiamandate', None, 'Lietuvos lenkų rinkimų akcija-Krikščioniškų šeimų sąjunga'),
        (43, 'daugiamandate', 'Regina Ablom', 'Lietuvos lenkų rinkimų akcija-Krikščioniškų šeimų sąjunga'),
        (7, 'daugiamandate', None, 'Lietuvos Respublikos liberalų sąjūdis'),
        (115, 'daugiamandate', 'Inga Abrutytė', 'Lietuvos Respublikos liberalų sąjūdis'),
        (2, 'daugiamandate', None, 'Tėvynės sąjunga - Lietuvos krikščionys demokratai'),
        (69, 'daugiamandate', 'Kęstutis Ačas', 'Tėvynės sąjunga - Lietuvos krikščionys demokratai'),
        (None, 'vienmandate', 'Remigijus Ačas', 'Raseinių-Kėdainių'),
        (6, 'daugiamandate', None, 'Lietuvos valstiečių ir žaliųjų sąjunga'),
        (45, 'daugiamandate', 'Vida Ačienė', 'Lietuvos valstiečių ir žaliųjų sąjunga'),
        (1, 'daugiamandate', None, 'Lietuvos socialdemokratų partija'),
        (59, 'daugiamandate', 'Dovilė Adamonytė', 'Lietuvos socialdemokratų partija'),
        (5, 'daugiamandate', None, 'Partija Tvarka ir teisingumas'),
        (88, 'daugiamandate', 'Algimantas Adomaitis', 'Partija Tvarka ir teisingumas'),
        (132, 'daugiamandate', 'Kasparas Adomaitis', 'Lietuvos Respublikos liberalų sąjūdis'),
        (14, 'daugiamandate', 'Mantas Adomėnas', 'Tėvynės sąjunga - Lietuvos krikščionys demokratai'),
        (None, 'vienmandate', 'Mantas Adomėnas', 'Kaišiadorių-Elektrėnų'),
        (14, 'daugiamandate', None, 'Antikorupcinė N. Puteikio ir K. Krivicko koalicija (Lietuvos centro partija, Lietuvos pensininkų partiją)'),
        (26, 'daugiamandate', 'Gediminas Akelaitis', 'Antikorupcinė N. Puteikio ir K. Krivicko koalicija (Lietuvos centro partija, Lietuvos pensininkų partiją)'),
        (None, 'vienmandate', 'Gediminas Akelaitis', 'Marijampolės'),
    ]
