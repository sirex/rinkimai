env/installed: requirements-dev.txt Makefile
	env/bin/pip install -r requirements-dev.txt -e .
	touch env/installed

requirements-dev.txt: env/bin/pip-compile requirements-dev.in requirements.in
	env/bin/pip-compile requirements-dev.in requirements.in -o requirements-dev.txt
	env/bin/pip-compile requirements.in -o requirements.txt

env/bin/pip-compile:
	env/bin/pip install pip-tools