# Prepare development environment
make

# Activate virtualenv
source env/bin/activate

# Create initial migration
./manage.py makemigrations rinkimai

# Run migrations
./manage.py migrate

# Run server
./manage.py runserver

# Create new data migration
./manage.py makemigrations --empty rinkimai

# Import candidates 2016
./manage.py importcandidates 2016

# Generate requirements.txt
env/bin/pip-compile requirements.in -o requirements.txt


# 2020-08-03 11:50 Rollback 0009 migration and run migrations again.
./manage.py showmigrations rinkimai
./manage.py migrate rinkimai 0009_quote_approved_by
./manage.py migrate rinkimai


# 2020-08-03 15:04 Add data migration for group abbrs and slugs.
# Created a better abbreviation function and want to update all group
# abbreviations suing this improved function.
./manage.py makemigrations --empty rinkimai
./manage.py createtestdata
./manage.py showmigrations rinkimai
./manage.py migrate rinkimai


# 2020-08-03 17:02 Check why my-candidates page is so slow
# My favorite profiler https://github.com/what-studio/profiling
pip install profiling
profiling --sampling manage.py -- runserver --nothreading --noreload
# It turns out, that constructing Django model instances is a very expensive
# thing. Adding `values_list` improved performance many times.


# 2020-08-03 20:37 Check total number of reactions

ssh root@109.235.67.172

sudo -u renkam psql <<EOF
SELECT COUNT(*) FROM rinkimai_reaction;
EOF
#|  count
#| -------
#|      9
#| (1 row)


# 2020-08-04 09:22 Get data from LinkedIn

# I want to get data aboult this comment:
# https://www.linkedin.com/feed/update/urn:li:article:8997185398901410626?commentUrn=urn%3Ali%3Acomment%3A%28article%3A8997185398901410626%2C6234286828028067840%29
# Found this API documentation:
# https://docs.microsoft.com/en-us/linkedin/marketing/integrations/community-management/shares/network-update-social-actions
http https://api.linkedin.com/v2/socialActions/urn%3Ali%3Acomment%3A%28article%3A8997185398901410626%2C6234286828028067840%29
#| HTTP/1.1 401 Unauthorized
#|
#| {
#|     "message": "Empty oauth2 access token",
#|     "serviceErrorCode": 65604,
#|     "status": 401
#| }
# Docs on auth:
# https://docs.microsoft.com/lt-lt/linkedin/shared/authentication/client-credentials-flow
# Had to create a page:
# https://www.linkedin.com/company/renkam-lt/
# And an app for that page:
# https://www.linkedin.com/developers/apps/30297863/settings
source secrets.env
http -f https://www.linkedin.com/oauth/v2/accessToken grant_type=client_credentials client_id=$linkedin_client_id client_secret=$linkedin_client_secret
#| HTTP/1.1 401 Unauthorized
#|
#| {
#|     "error": "access_denied",
#|     "error_description": "This application is not allowed to create application tokens"
#| }
# Found this on LinkedIn documentation.
#> Your application cannot access these APIs by default. Learn more
#> about LinkedIn Developer Enterprise products to request permission to
#> the Client Credential Flow.
# So it seems, that I cand user client credentials flow and must use
# authorization code flow (3-legged OAuth).
# https://docs.microsoft.com/lt-lt/linkedin/shared/authentication/authorization-code-flow
xdg-open "https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=$linkedin_client_id&redirect_uri=http%3A%2F%2F127.0.0.1%2Foauth%2Flinkedin%2Fcallback&scope=r_liteprofile%20w_member_social"
python - 'http://127.0.0.1:8000/oauth/linkedin/callback?error=unauthorized_scope_error&error_description=Scope+%26quot%3Br_liteprofile%26quot%3B+is+not+authorized+for+your+application' <<EOF
import sys, html, urllib.parse
urlp = urllib.parse.urlparse(sys.argv[1])
for k, v in urllib.parse.parse_qsl(urlp.query):
    v = urllib.parse.unquote_plus(v)
    v = html.unescape(v)
    print(f'{k} = {v}')
EOF
#| error = unauthorized_scope_error
#| error_description = Scope "r_liteprofile" is not authorized for your application
xdg-open "https://www.linkedin.com/oauth/v2/authorization?response_type=code&client_id=$linkedin_client_id&redirect_uri=http%3A%2F%2F127.0.0.1%2Foauth%2Flinkedin%2Fcallback&scope=w_member_social"
python - 'https://127.0.0.1/oauth/linkedin/callback?error=unauthorized_scope_error&error_description=Scope+%26quot%3Bw_member_social%26quot%3B+is+not+authorized+for+your+application' <<EOF
import sys, html, urllib.parse
urlp = urllib.parse.urlparse(sys.argv[1])
for k, v in urllib.parse.parse_qsl(urlp.query):
    v = urllib.parse.unquote_plus(v)
    v = html.unescape(v)
    print(f'{k} = {v}')
EOF
#| error = unauthorized_scope_error
#| error_description = Scope "w_member_social" is not authorized for your application
# OK, nothing works, I'm giving up.
# I'm giving up with LinkedIn API, but maybe it is possible to extract
# comment data directly from HTML?
xdg-open 'https://www.linkedin.com/feed/update/urn:li:article:8997185398901410626?commentUrn=urn%3Ali%3Acomment%3A%28article%3A8997185398901410626%2C6234286828028067840%29'


# 2020-08-04 10:51 Remove .idea from git

git rm --cached -r .idea

