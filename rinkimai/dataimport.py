import datetime
from itertools import islice

import requests
import xlrd
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.utils.text import slugify

from rinkimai.models import Candidate
from rinkimai.models import Election
from rinkimai.models import Group
from rinkimai.models import Person
from rinkimai.utils.names import abbreviate


def ensure_import_dir(year: int, kind: str):
    import_dir = settings.IMPORT_DIR / f'{year}-{kind}'
    import_dir.mkdir(parents=True, exist_ok=True)
    return import_dir


def download_file(year, kind, filename, url):
    file = ensure_import_dir(year, kind) / filename
    if not file.exists():
        with requests.get(url, stream=True) as r:
            r.raise_for_status()
            with file.open('wb') as f:
                for chunk in r.iter_content(chunk_size=1024 * 10):
                    f.write(chunk)
    return file


def normalize_name_case(name: str):
    return ' '.join([name.title() for name in name.split()])


def get_or_create_election(year: int, kind: Election.Kind) -> Election:
    try:
        return Election.objects.get(date__year=year, kind=kind)
    except ObjectDoesNotExist:
        return Election.objects.create(
            date=datetime.date(year, 1, 1),
            kind=kind,
        )


def import_2016_parliament_elections(*, limit=None):
    file = download_file(
        2016, 'parliament', 'Kandidatai+pagal+ABC+12-30+09-15.xlsx',
        'http://www.vrk.lt/documents/10180/606155/Kandidatai+pagal+ABC+12-30+09-15.xlsx/1a458fb4-bcb2-4ecf-8760-b5e8c57cb187',
    )

    election = get_or_create_election(2016, Election.Kind.PARLIAMENT)

    rows = read_xlsx(str(file))
    header = next(rows)
    if limit is not None:
        rows = islice(rows, limit)
    for row in rows:
        row = dict(zip(header, row))
        slug = slugify(row['Vardas PAVARDĖ'])[:50]
        person, _ = Person.objects.get_or_create(slug=slug, defaults={
            'slug': slugify(row['Vardas PAVARDĖ']),
            'name': normalize_name_case(row['Vardas PAVARDĖ']),
        })

        if row['Daugiamandatės sąrašas']:
            abbr = abbreviate(row['Daugiamandatės sąrašas'])
            group, _ = Group.objects.get_or_create(
                slug=slugify(abbr),
                name=row['Daugiamandatės sąrašas'],
                defaults={
                    'abbr': abbr,
                }
            )
            defaults = {
                'kind': Candidate.Kind.DAUGIAMANDATE,
            }
            if row['Daugiamandatės sąrašo Nr.']:
                defaults['number'] = int(row['Daugiamandatės sąrašo Nr.'])
            Candidate.objects.get_or_create(
                election=election,
                person=None,
                group=group,
                defaults=defaults
            )

            defaults = {
                'kind': Candidate.Kind.DAUGIAMANDATE,
            }
            if row['Nr. sąraše']:
                defaults['number'] = int(row['Nr. sąraše'])
            Candidate.objects.get_or_create(
                election=election,
                person=person,
                group=group,
                defaults=defaults,
            )

        if row['Vienmandatė apygarda']:
            group, _ = Group.objects.get_or_create(
                slug=slugify(row['Vienmandatė apygarda']),
                name=row['Vienmandatė apygarda'],
            )
            Candidate.objects.get_or_create(
                election=election,
                person=person,
                group=group,
                defaults={
                    'kind': Candidate.Kind.VIENMANDATE,
                },
            )


def read_xlsx(filename):
    wb = xlrd.open_workbook(filename)
    ws = wb.sheet_by_index(0)

    def _parse_cell(value, typ):
        if typ == xlrd.XL_CELL_ERROR:
            value = None
        elif typ == xlrd.XL_CELL_BOOLEAN:
            value = bool(value)
        elif typ == xlrd.XL_CELL_NUMBER:
            # GH5394 - Excel 'numbers' are always floats
            # it's a minimal perf hit and less surprising
            val = int(value)
            if val == value:
                value = val

        return value

    for i in range(ws.nrows):
        yield [
            _parse_cell(value, typ)
            for value, typ in zip(ws.row_values(i), ws.row_types(i))
        ]
