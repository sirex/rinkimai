"""rinkimai URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path

from rinkimai.views import add_group
from rinkimai.views import add_person
from rinkimai.views import add_quote
from rinkimai.views import approve_quote
from rinkimai.views import group_quotes
from rinkimai.views import person_quotes
from rinkimai.views import index
from rinkimai.views import quote_details
from rinkimai.views import reactions
from rinkimai.views import save_reaction
from rinkimai.views import tag_quotes
from rinkimai.views import unapproved_quotes
from rinkimai.views import voter_candidates

urlpatterns = [
    path('', index, name='home'),
    path('add-quote', add_quote, name="add-quote"),
    path('add-person', add_person, name='add-person'),
    path('add-group', add_group, name='add-group'),
    path('save-reaction', save_reaction, name='save-reaction'),
    path('approve-quote', approve_quote, name='approve-quote'),
    path('unapproved-quotes', unapproved_quotes, name='unapproved-quotes'),
    path('reactions', reactions, name="my-reactions"),
    path('my-candidates', voter_candidates, name="my-candidates"),
    path('quote/<int:quote_id>', quote_details, name='quote-details'),
    path('person/<str:slug>', person_quotes, name='person-quotes'),
    path('group/<str:slug>', group_quotes, name='group-quotes'),
    path('tag/<str:tag>', tag_quotes, name='tag-quotes'),
    path('valdymas/', admin.site.urls),
]
