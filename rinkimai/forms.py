from django import forms
from django.forms import Form
from django.forms.models import ModelForm

from rinkimai.models import Group
from rinkimai.models import Person
from rinkimai.models import Quote
from rinkimai.models import Reaction


class SaveReactionForm(ModelForm):

    class Meta:
        model = Reaction
        fields = [
            'quote',
            'reaction',
        ]


class AskQuoteSourceForm(Form):

    source = forms.URLField()


class AddQuoteForm(ModelForm):

    class Meta:
        model = Quote
        fields = [
            'source',
            'person',
            'group',
            'date',
            'text',
            'tags',
        ]


class PersonForm(ModelForm):

    class Meta:
        model = Person
        fields = [
            'name',
            'bday',
        ]


class GroupForm(ModelForm):

    class Meta:
        model = Group
        fields = [
            'name',
            'abbr',
        ]


class ApproveQuoteForm(ModelForm):

    class Meta:
        model = Quote
        fields = [
            'active',
        ]
