from typing import Callable

from django.http.request import HttpRequest
from django.http.response import HttpResponse

from rinkimai.utils.voter import get_voter


def voter(get_response: Callable) -> Callable:

    def handler(request: HttpRequest) -> HttpResponse:
        request.voter = get_voter(request)
        response = get_response(request)
        request.voter.set_cookies(response)
        return response

    return handler
