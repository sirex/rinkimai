from django.contrib import admin
from django.forms.models import BaseInlineFormSet
from django.forms.models import inlineformset_factory
from django.forms.models import modelform_factory

from rinkimai.models import Candidate
from rinkimai.models import Election
from rinkimai.models import Group
from rinkimai.models import Person
from rinkimai.models import Quote
from rinkimai.models import Settings


@admin.register(Settings)
class SettingsAdmin(admin.ModelAdmin):
    list_display = ('name_in_list',)
    list_select_related = ('election',)

    def name_in_list(self, obj):
        return f"Settings(election=<{obj.election}>)"


@admin.register(Election)
class ElectionAdmin(admin.ModelAdmin):
    pass


@admin.register(Candidate)
class CandidateAdmin(admin.ModelAdmin):
    list_display = ('person', 'group', 'election')
    list_select_related = ('election', 'person', 'group')
    search_fields = ['name', 'person__name', 'group__name']


class CandidateInlineFormSet(BaseInlineFormSet):

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.queryset = (
            (self.queryset or Candidate.objects.all()).
            filter(person__isnull=True)
        )

    # def get_queryset(self):
    #     if not hasattr(self, '_queryset'):
    #         qs = super().get_queryset().filter(person__isnull=True)
    #         self._queryset = qs
    #     return self._queryset


class CandidateInline(admin.TabularInline):
    model = Candidate
    formset = CandidateInlineFormSet
    extra = 0
    fields = ('election', 'number')


@admin.register(Group)
class GroupAdmin(admin.ModelAdmin):
    list_display = ('abbr', 'name')
    search_fields = ['abbr', 'name']
    inlines = [CandidateInline]


@admin.register(Person)
class PersonAdmin(admin.ModelAdmin):
    search_fields = ['name']


@admin.register(Quote)
class QuoteAdmin(admin.ModelAdmin):
    list_display = ('text', 'person', 'date')
    list_select_related = ('person',)
    search_fields = ['text', 'person__name']
    raw_id_fields = ['quote']
