import datetime

import factory
from django.utils.text import slugify

from rinkimai.models import Candidate
from rinkimai.models import Election
from rinkimai.models import Group
from rinkimai.models import Moderator
from rinkimai.models import Person
from rinkimai.models import Quote
from rinkimai.models import Reaction
from rinkimai.models import Settings
from rinkimai.utils.names import abbreviate

DEFAULT_ELECTION_DATE = datetime.date(2020, 10, 11)


class GroupFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = Group
        django_get_or_create = ('name',)

    name = 'Darbo partija'
    abbr = factory.LazyAttribute(lambda o: abbreviate(o.name))
    slug = factory.LazyAttribute(lambda o: slugify(o.abbr or o.name))
    aliases = factory.LazyFunction(list)

    candidate = factory.RelatedFactory(
        'rinkimai.testing.factories.GroupCandidateFactory',
        factory_related_name='group',
    )


class PersonFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = Person
        django_get_or_create = ('name',)

    name = factory.Faker('name', locale='lt_LT')
    slug = factory.LazyAttribute(lambda o: slugify(o.name))
    bday = factory.Faker('date_of_birth', minimum_age=18)
    aliases = factory.LazyFunction(list)

    candidate = factory.RelatedFactory(
        'rinkimai.testing.factories.CandidateFactory',
        factory_related_name='person',
    )


class ElectionFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = Election
        django_get_or_create = ('date', 'kind')

    date = factory.Faker('date_between', start_date='-30y', end_date='5y')
    kind = Election.Kind.PARLIAMENT


class CandidateFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = Candidate
        django_get_or_create = ('election', 'person', 'group')

    election = factory.SubFactory(ElectionFactory, date=DEFAULT_ELECTION_DATE)
    name = factory.Faker('name', locale='lt_LT')
    person = factory.SubFactory(PersonFactory, name=factory.SelfAttribute('..name'))
    group = factory.SubFactory(GroupFactory)
    number = factory.Iterator(range(1, 140))


class GroupCandidateFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = Candidate
        django_get_or_create = ('election', 'person', 'group')

    election = factory.SubFactory(ElectionFactory, date=DEFAULT_ELECTION_DATE)
    name = factory.Faker('name', locale='lt_LT')
    person = None
    group = factory.SubFactory(GroupFactory, name=factory.SelfAttribute('..name'))
    number = factory.Iterator(range(1, 140))


class QuoteFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = Quote
        django_get_or_create = ('text',)

    quote = None
    group = factory.SubFactory(GroupFactory)
    person = factory.SubFactory(PersonFactory)
    source = factory.Faker('url')
    date = factory.Faker('date_between', start_date='-30y')
    text = factory.Faker('paragraph', locale='lt_LT')
    tags = factory.LazyFunction(list)
    reaction = 0
    active = True


class ReactionFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = Reaction
        django_get_or_create = ('private_key', 'quote')

    private_key = factory.Faker('uuid4')
    public_key = factory.Faker('uuid4')
    quote = factory.SubFactory(QuoteFactory)
    reaction = 0
    tags = factory.LazyFunction(list)


class ModeratorFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = Moderator
        django_get_or_create = ('name',)

    public_key = factory.Faker('uuid4')
    name = factory.Faker('name', locale='lt_LT')
    active = True


class SettingsFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = Settings

    election = factory.SubFactory(ElectionFactory, date=DEFAULT_ELECTION_DATE)
