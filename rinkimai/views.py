from typing import List
from typing import Optional
from urllib.parse import urlencode

from django.contrib.admin.views.decorators import staff_member_required
from django.core.exceptions import ObjectDoesNotExist
from django.db.models import Q
from django.http import HttpResponseRedirect
from django.http.request import HttpRequest
from django.http.response import Http404
from django.http.response import JsonResponse
from django.shortcuts import get_object_or_404
from django.template.response import TemplateResponse
from django.urls import reverse
from django.utils.text import slugify
from django.views.decorators.http import require_http_methods

from rinkimai.forms import AddQuoteForm
from rinkimai.forms import ApproveQuoteForm
from rinkimai.forms import AskQuoteSourceForm
from rinkimai.forms import GroupForm
from rinkimai.forms import PersonForm
from rinkimai.forms import SaveReactionForm
from rinkimai.helpers import extract_source_data
from rinkimai.helpers import suggested_person_or_group_for_quote
from rinkimai.models import Group
from rinkimai.models import Person
from rinkimai.models import Quote
from rinkimai.models import Reaction
from rinkimai.services import get_current_election
from rinkimai.services import query_quotes
from rinkimai.services import query_voter_quotes
from rinkimai.utils.dates import normalize_date
from rinkimai.utils.voter import Voter
from rinkimai.services import query_voter_candidates


def index(request: HttpRequest):
    voter: Voter = request.voter
    election = get_current_election()
    return TemplateResponse(request, 'index.html', {
        'voter': voter,
        'quotes': query_quotes(election, voter, user=request.user),
        'active_menu_item': 'home',
        'add_new_quote_url': reverse('add-quote'),
    })


@require_http_methods(['POST'])
def save_reaction(request: HttpRequest):
    voter: Voter = request.voter
    try:
        reaction = Reaction.objects.get(
            private_key=voter.private_key,
            quote=request.POST.get('quote'),
        )
    except ObjectDoesNotExist:
        reaction = None
    form = SaveReactionForm(request.POST, instance=reaction)
    if form.is_valid():
        reaction = form.save(commit=False)
        reaction.private_key = voter.private_key
        reaction.public_key = voter.public_key
        reaction.save()
        return JsonResponse({'errors': None})
    else:
        return JsonResponse({'errors': form.errors.get_json_data()})


def reactions(request: HttpRequest):
    voter: Voter = request.voter
    return TemplateResponse(request, 'reactions.html', {
        'voter': voter,
        'quotes': query_voter_quotes(voter),
        'active_menu_item': 'my-reactions',
        'add_new_quote_url': reverse('add-quote'),
    })


@require_http_methods(['GET', 'POST'])
def add_quote(request: HttpRequest):
    if request.method == 'GET':
        source = request.GET.get('source')
        person = request.GET.get('person')
        group = request.GET.get('group')
        if source is None:
            form = AskQuoteSourceForm()
            return TemplateResponse(request, 'ask_quote_source.html', {
                'form': form,
                'person': person,
                'group': group,
            })
        else:
            data = extract_source_data(source)
            names = data['names']
            if person:
                names += [person]
            if group:
                names += [group]
            names = suggested_person_or_group_for_quote(names)
            form = AddQuoteForm(initial={
                'source': source,
                'date': data['date'],
            })
            return TemplateResponse(request, 'add_quote.html', {
                'form': form,
                'names': names,
                'source': source,
            })
    else:
        post = request.POST.copy()

        if post['date']:
            post['date'] = normalize_date(post['date'])

        author = post.get('suggested_author', '')
        if ':' in author:
            author_type, author = author.split(':', 1)
            if author_type == 'person':
                person = author
                group = ''
            else:
                person = ''
                group = author
        else:
            author_type = author
            person = ''
            group = ''

        if author_type == 'person':
            post['person'] = person = person or post.get('person', '')
            post['group'] = group = ''
        else:
            post['person'] = person = ''
            post['group'] = group = group or post.get('group', '')

        if person.isdigit():
            person = ''
        elif person:
            found: List[Person] = list(
                Person.objects.
                only('pk', 'name').
                filter(Q(name=person) | Q(aliases__contains=[person]))
            )

            # If one person found, just set id of that person.
            if len(found) == 1:
                post['person'] = found[0].pk

            # If more than one person found, return back to form and ask to
            # select which person.
            elif len(found) > 1:
                names = [p.name for p in found]
                names = suggested_person_or_group_for_quote(names)
                form = AddQuoteForm(post)
                return TemplateResponse(request, 'add_quote.html', {
                    'form': form,
                    'names': names,
                    'source': post.get('source'),
                })

            # If person is not found, save Quote and proceed to the next Add
            # Person form.
            else:
                post['person'] = ''

        if group.isdigit():
            group = ''
        elif group:
            found: List[Group] = list(
                Group.objects.
                only('pk', 'name').
                filter(
                    Q(name__icontains=group) |
                    Q(abbr=group) |
                    Q(aliases__contains=[group])
                )
            )

            # If one group found, just set id of that group.
            if len(found) == 1:
                post['group'] = found[0].pk

            # If more than one group found, return back to form and ask to
            # select which group.
            elif len(found) > 1:
                names = [p.name for p in found]
                names = suggested_person_or_group_for_quote(names)
                form = AddQuoteForm(post)
                return TemplateResponse(request, 'add_quote.html', {
                    'form': form,
                    'names': names,
                    'source': post.get('source'),
                })

            # If group is not found, save Quote and proceed to the next Add
            # Group form.
            else:
                post['group'] = ''

        form = AddQuoteForm(post)
        if form.is_valid():
            voter: Voter = request.voter
            quote: Quote = form.save(commit=False)
            quote.private_key = voter.private_key
            quote.active = False
            quote.save()

            # Add new person.
            if author_type == 'person':
                if quote.person is None:
                    params = {'quote': quote.pk}
                    if person:
                        params['person'] = person
                    return HttpResponseRedirect(
                        reverse('add-person') + '?' + urlencode(params)
                    )

            # Add new group.
            else:
                if quote.group is None:
                    params = {'quote': quote.pk}
                    if group:
                        params['group'] = group
                    return HttpResponseRedirect(
                        reverse('add-group') + '?' + urlencode(params)
                    )

            return HttpResponseRedirect(quote.get_absolute_url())

        else:
            source = post.get('source')
            data = extract_source_data(source)
            names = suggested_person_or_group_for_quote(data['names'])
            return TemplateResponse(request, 'add_quote.html', {
                'form': form,
                'names': names,
                'source': source,
            })


@require_http_methods(['GET', 'POST'])
def add_person(request: HttpRequest):
    voter: Voter = request.voter
    quote: Optional[Quote] = None
    if 'quote' in request.GET:
        quote = get_object_or_404(
            Quote,
            pk=request.GET['quote'],
            private_key=voter.private_key,
        )

    if request.method == 'POST':
        form = PersonForm(request.POST)
        if form.is_valid():
            person = form.save(commit=False)
            person.slug = slugify(person.name)[:50]
            person.save()
            if quote:
                quote.person = person
                quote.group = None
                quote.save()
                return HttpResponseRedirect(quote.get_absolute_url())
            else:
                return HttpResponseRedirect(reverse('home'))
    else:
        form = PersonForm(initial={
            'name': request.GET.get('person', ''),
        })
    return TemplateResponse(request, 'add_person.html', {
        'form': form,
    })


@require_http_methods(['GET', 'POST'])
def add_group(request: HttpRequest):
    voter: Voter = request.voter
    quote: Optional[Quote] = None
    if 'quote' in request.GET:
        quote = get_object_or_404(
            Quote,
            pk=request.GET['quote'],
            private_key=voter.private_key,
        )

    if request.method == 'POST':
        form = GroupForm(request.POST)
        if form.is_valid():
            group = form.save(commit=False)
            group.slug = slugify(group.name)[:50]
            group.save()
            if quote:
                quote.person = None
                quote.group = group
                quote.save()
                return HttpResponseRedirect(quote.get_absolute_url())
            else:
                return HttpResponseRedirect(reverse('home'))
    else:
        form = PersonForm(initial={
            'name': request.GET.get('group', ''),
        })
    return TemplateResponse(request, 'add_group.html', {
        'form': form,
    })


@require_http_methods(['GET'])
def quote_details(request: HttpRequest, quote_id: int):
    voter: Voter = request.voter
    election = get_current_election()
    quotes = query_quotes(election, voter, quote=quote_id, user=request.user)
    if not quotes:
        raise Http404("No active quotes are found.")
    return TemplateResponse(request, 'quote_details.html', {
        'quotes': quotes,
        'add_new_quote_url': reverse('add-quote'),
    })


def person_quotes(request: HttpRequest, slug: str):
    voter: Voter = request.voter
    election = get_current_election()
    person = get_object_or_404(Person, slug=slug)
    quotes = query_quotes(election, voter, person=person, user=request.user)
    return TemplateResponse(request, 'person_quotes.html', {
        'quotes': quotes,
        'add_new_quote_url': reverse('add-quote') + '?' + urlencode({
            'person': person.name,
        })
    })


def group_quotes(request: HttpRequest, slug: str):
    voter: Voter = request.voter
    election = get_current_election()
    group = get_object_or_404(Group, slug=slug)
    quotes = query_quotes(election, voter, group=group, user=request.user)
    return TemplateResponse(request, 'group_quotes.html', {
        'quotes': quotes,
        'add_new_quote_url': reverse('add-quote') + '?' + urlencode({
            'group': group.name,
        })
    })


def tag_quotes(request: HttpRequest, tag: str):
    voter: Voter = request.voter
    election = get_current_election()
    quotes = query_quotes(election, voter, tag=tag, user=request.user)
    return TemplateResponse(request, 'tag_quotes.html', {
        'quotes': quotes,
        'add_new_quote_url': reverse('add-quote'),
    })


@staff_member_required(login_url='home', redirect_field_name=None)
def approve_quote(request: HttpRequest):
    quote = get_object_or_404(Quote, pk=request.POST.get('quote'))
    form = ApproveQuoteForm(request.POST, instance=quote)
    if form.is_valid():
        quote = form.save(commit=False)
        if quote.active:
            quote.approved_by = request.user
        quote.save()
        return JsonResponse({'errors': None})
    else:
        return JsonResponse({'errors': form.errors.get_json_data()})


@staff_member_required(login_url='home', redirect_field_name=None)
def unapproved_quotes(request: HttpRequest):
    voter: Voter = request.voter
    election = get_current_election()
    return TemplateResponse(request, 'index.html', {
        'voter': voter,
        'quotes': query_quotes(election, voter, active=False, user=request.user),
        'active_menu_item': 'unapproved-quotes',
        'add_new_quote_url': reverse('add-quote'),
    })


def voter_candidates(request: HttpRequest):
    voter: Voter = request.voter
    election = get_current_election()
    return TemplateResponse(request, 'voter-candidates.html', {
        'voter': voter,
        'candidates': query_voter_candidates(election, voter),
        'active_menu_item': 'my-candidates',
        'add_new_quote_url': reverse('add-quote'),
    })
