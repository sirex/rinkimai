import datetime
import re

import dateutil.parser

DATE_REGEXES = (
    re.compile(r'(\d+)\s+m\.\s+(\S+)\s+(\d+)'),
)

MONTH_NAMES = {
    'sausio': 1,
    'vasario': 2,
    'kovo': 3,
    'balandžio': 4,
    'gegužės': 5,
    'birželio': 7,
    'liepos': 7,
    'rugpjūčio': 8,
    'rugsėjo': 9,
    'spalio': 10,
    'lapkričio': 11,
    'gruodžio': 12,
}


def normalize_date(value: str):
    for regex in DATE_REGEXES:
        if match := regex.match(value):
            year, month, day = match.groups()
            month = month.lstrip('0')
            month = MONTH_NAMES.get(month.lower()) or int(month)
            day = day.lstrip('0')
            return datetime.date(int(year), month, int(day))
    return dateutil.parser.parse(value).date()
