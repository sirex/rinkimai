import importlib
import uuid
from typing import Tuple

from django.conf import settings
from django.http.request import HttpRequest
from django.http.response import HttpResponse


class Voter:
    private_key: str = None
    public_key: str = None

    _set_cookies: bool = False

    def read_cookies(self, request: HttpRequest):
        self.private_key = request.get_signed_cookie('private_key', None)
        self.public_key = request.get_signed_cookie('public_key', None)
        self._set_cookies = False

        if self.private_key is None or self.public_key is None:
            self.gen_key_pair()
            self._set_cookies = True

    def gen_key_pair(self) -> None:
        self.private_key = str(uuid.uuid4())
        self.public_key = str(uuid.uuid4())

    def set_cookies(self, response: HttpResponse) -> None:
        if self._set_cookies:
            response.set_signed_cookie('private_key', self.private_key)
            response.set_signed_cookie('public_key', self.public_key)

    def set_key_pair(
        self,
        private_key: str = None,
        public_key: str = None,
    ) -> None:
        self.private_key = private_key or str(uuid.uuid4())
        self.public_key = public_key or str(uuid.uuid4())


class TestVoter(Voter):

    def gen_key_pair(self) -> None:
        self.private_key = 'PRIVATE'
        self.public_key = 'PUBLIC'


def get_voter(request: HttpRequest) -> Voter:
    import_path = settings.RINKIMAI_USER_MANAGER
    module, klass = import_path.split(':', 1)
    module = importlib.import_module(module)
    VoterClass = getattr(module, klass)
    voter = VoterClass()
    voter.read_cookies(request)
    return voter
