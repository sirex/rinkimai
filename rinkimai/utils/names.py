import re
from typing import Dict
from typing import Iterable
from typing import Iterator
from typing import List

from rinkimai.models import Person

NamesTree = Dict[str, dict]

words_re = re.compile(r'\W+')


def find_people_names(tree: NamesTree, text: str) -> List[str]:
    result = set()
    name = []
    names = tree

    for word in filter(None, words_re.split(text)):
        if word.lower() in names:
            name += [word]
            names = names[word.lower()]
            if not names:
                result.add(' '.join(name))
                name = []
                names = tree
        elif name:
            name = []
            names = tree
    return sorted(result)


def prepare_name_list(names: Iterable[str]) -> NamesTree:
    tree: NamesTree = {}
    for name in names:
        words = name.lower().split()
        node = tree
        for word in words:
            if word not in node:
                node[word] = {}
            node = node[word]
    return tree


def iter_all_people_names() -> Iterator[str]:
    for name, aliases in Person.objects.values_list('name', 'aliases'):
        yield name
        yield from aliases


def abbreviate(name: str) -> str:
    repl = [
        ('"', ''),
        ('“', ''),
        (',', ''),
        ('-', ' - '),
    ]
    for k, s in repl:
        name = name.replace(k, s)

    if ' ' in name:
        buff = []
        words = []
        for w in name.split():
            # Add `-` before `„` and remove `„`.
            if w.startswith('„'):
                w = w[1:]
                if words:
                    words.append('-')

            # Remove name initials, like in `F. Last`. Here `F.` will be
            # removed.
            if len(w) == 2 and w.endswith('.'):
                continue

            # Ignore everything that comes after open paren.
            if words and w.startswith('('):
                break

            # Normalize case.
            w = w.upper()

            # Ignore conjuctions.
            if w in ('IR',):
                continue

            buff.append(w)

            # Ignore `Politinė Partija`.
            if buff == ['POLITINĖ']:
                continue
            if buff == ['POLITINĖ', 'PARTIJA']:
                buff = []
                continue

            words += w[0]
            buff = []
        return ''.join(words)
    else:

        # Single word names return as a whole name. Usually such names are used
        # in tests, like P1, P2, ...
        return name.upper()
