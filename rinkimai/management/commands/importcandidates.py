from django.core.management.base import BaseCommand, CommandError

from rinkimai.dataimport import import_2016_parliament_elections
from rinkimai.models import Election


class Command(BaseCommand):
    help = 'Import election candidates'

    def add_arguments(self, parser):
        parser.add_argument('year', type=int)
        parser.add_argument('kind', choices=Election.Kind.values, nargs='?')
        parser.add_argument('--limit', type=int)

    def handle(self, *args, **options):
        importers = {
            (2016, 'parliament'): import_2016_parliament_elections,
        }

        if options['kind'] is None:
            kinds = [
                kind
                for year, kind in importers
                if year == options['year']
            ]
            if len(kinds) > 1:
                kinds = ', '.join(kinds)
                raise CommandError(
                    f"In {options['year']} there was more than one election,"
                    "please specify what kind of election you are referring "
                    f"to: {kinds}."
                )
            if len(kinds) == 0:
                raise CommandError(
                    f"There are no importers for {options['year']} elections."
                )
            kind = kinds[0]
        else:
            kind = options['kind']
        year = options['year']

        if (year, kind) not in importers:
            raise CommandError(
                f"There are no importers for {year} ({kind}) elections."
            )

        importer = importers[(year, kind)]
        importer(limit=options['limit'])
