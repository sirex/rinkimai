import datetime

from django.contrib.auth.models import User
from django.core.management.base import BaseCommand

from rinkimai.models import Election
from rinkimai.testing.factories import ElectionFactory
from rinkimai.testing.factories import GroupFactory
from rinkimai.testing.factories import PersonFactory
from rinkimai.testing.factories import QuoteFactory
from rinkimai.testing.factories import SettingsFactory


class Command(BaseCommand):
    help = "Import test data"

    def handle(self, *args, **options):
        if not User.objects.filter(username='admin').exists():
            User.objects.create_superuser(
                'admin',
                'admin@example.com',
                'admin',
            )

        election = ElectionFactory(
            date=datetime.date(2016, 10, 9),
            kind=Election.Kind.PARLIAMENT,
        )

        SettingsFactory(
            election=election,
        )

        GroupFactory(name="Lietuvos žaliųjų partija")
        GroupFactory(name="Lietuvos centro partija")
        GroupFactory(name="Darbo partija")
        GroupFactory(name="Lietuvos valstiečių ir žaliųjų sąjunga")
        GroupFactory(name="Tėvynės sąjunga - Lietuvos krikščionys demokratai")
        GroupFactory(name="Lietuvos socialdemokratų partija")
        GroupFactory(name="Lietuvos Respublikos liberalų sąjūdis")
        GroupFactory(name="Lietuvos lenkų rinkimų akcija-Krikščioniškų šeimų sąjunga")
        GroupFactory(name="Partija Tvarka ir teisingumas")
        GroupFactory(name="Antikorupcinė N. Puteikio ir K. Krivicko koalicija (Lietuvos centro partija, Lietuvos pensininkų partiją)")
        GroupFactory(name="Lietuvos liaudies partija")
        GroupFactory(name="Politinė partija „Lietuvos sąrašas“")
        GroupFactory(name="S. Buškevičiaus ir Tautininkų koalicija „Prieš korupciją ir skurdą“ (Partija „Jaunoji Lietuva“, Tautininkų sąjunga)")
        GroupFactory(name="„Drąsos kelias“ politinė partija")
        GroupFactory(name="Lietuvos laisvės sąjunga (liberalai)")

        paluckas = PersonFactory(name="Gintautas Paluckas")
        karbauskis = PersonFactory(name="Ramūnas Karbauskis")
        kubiliene = PersonFactory(name="Asta Kubilienė")

        QuoteFactory(
            person=paluckas,
            group=None,
            source='https://www.delfi.lt/news/daily/lithuania/paluckas-visi-kas-gali-dirbti-turi-dirbti.d?id=84843429',
            text="„Visi, kas gali dirbti, turi dirbti“, – Skandinavijos šalių socialdemokratinį šūkį partijos nariams priminė jis. „Kalbėdami apie gerovės valstybę nepamirškime visuotinio užimtumo, darbo vietos, tinkamos darbo užmokestis yra prielaida bet kokioms dosnioms socialinėms priemonės. Nes kitaip yra ne gerovės valstybė, o paskolinta valstybė“, – ironizavo G. Paluckas.",
            date=datetime.date(2020, 7, 24),
        )
        QuoteFactory(
            person=karbauskis,
            group=None,
            source='https://www.15min.lt/naujiena/aktualu/lietuva/r-karbauskis-svarstoma-galimybe-iteisinti-vicepremjeru-pareigybes-56-1350880',
            text="Šnekėdami apie pokyčius, mes keliame klausimą, kad galbūt (...) ne kurti naujų pareigybių, bet du ministrus patvirtinti ir vicepremjerais. Mes diskutuojame partijoje apie tai, kad būtų aišku, kurie ministrai gali pavaduoti ministrą pirmininką, kad tai nebūtų vienasmenis ministro pirmininko sprendimas.",
            date=datetime.date(2020, 7, 22),
        )
        QuoteFactory(
            person=karbauskis,
            group=None,
            source='https://www.15min.lt/naujiena/aktualu/lietuva/seimo-turtingiausieji-r-karbauskis-b-bradauskas-g-landsbergis-56-1347510',
            text="Kaip rodo Valstybinės mokesčių inspekcijos (VMI) skelbiamos pajamų ir turto deklaracijos, turtingiausias politikas Seime – Lietuvos valstiečių ir žaliųjų sąjungos pirmininkas Ramūnas Karbauskis.",
            date=datetime.date(2020, 7, 15),
        )
        QuoteFactory(
            person=kubiliene,
            group=None,
            source='https://www.15min.lt/naujiena/aktualu/lietuva/suabejojo-ar-verslo-konfederacijos-uzsakymu-tyrima-atlike-vu-mokslininkai-nepazeide-etikos-56-1351422',
            text="Turima daug netiesioginių duomenų, kad tyrimas pradėtas ir vykdomas būtent dėl Lietuvos alkoholio pramonės dalyvavimo ir rėmimo. Jeigu tai pasitvirtintų ir tyrimas išties yra finansuojamas tiesiogiai suinteresuotos alkoholio pramonės, tai tokio tyrimo vykdymas ir šios aplinkybės viešas nedeklaravimas būtų esminis akademinės etikos pažeidimas, o kartu ir keltų pamatines abejones dėl tyrimo metodikos ir jos pagrindu gautų duomenų.",
            date=datetime.date(2020, 7, 23),
        )
