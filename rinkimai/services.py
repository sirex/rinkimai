from typing import Callable
from typing import Dict
from typing import List
from typing import Optional
from typing import TypedDict

from django.contrib.auth.models import User
from django.db.models import Prefetch
from django.db.models import Q
from django.db.models.aggregates import Count
from django.db.models.aggregates import Sum
from django.db.models.expressions import Exists
from django.db.models.expressions import F
from django.db.models.expressions import OuterRef
from django.db.models.fields import FloatField
from django.db.models.functions.comparison import Cast
from django.db.models.query_utils import FilteredRelation

from rinkimai.helpers import get_source_name
from rinkimai.models import Candidate
from rinkimai.models import Election
from rinkimai.models import Group
from rinkimai.models import Person
from rinkimai.models import Quote
from rinkimai.models import Reaction
from rinkimai.utils.voter import Voter


def get_current_election():
    return Election.objects.filter(pk=F('settings__election')).first()


def query_quotes(
    election: Election,
    voter: Voter,
    *,
    quote: Optional[int] = None,
    person: Optional[Person] = None,
    group: Optional[Group] = None,
    tag: Optional[str] = None,
    user: User = None,
    active: bool = True,
) -> List[dict]:
    if quote is not None:
        qry = Quote.objects.filter(pk=quote)
    else:
        qry = (
            Quote.objects.
            # Query only top level quotes.
            filter(quote__isnull=True).
            # Exclude quotes where voter already voted.
            filter(~Exists(
                Reaction.objects.filter(
                    quote=OuterRef('pk'),
                    private_key=voter.private_key,
                )
            )).
            # Include only quotes from given election candidates.
            filter(Exists(
                Candidate.objects.filter(
                    Q(
                        person__isnull=False,
                        person=OuterRef('person'),
                        election=election,
                    ) |
                    Q(
                        person__isnull=True,
                        group__isnull=False,
                        group=OuterRef('group'),
                        election=election,
                    )
                )
            )).
            order_by('-created')
        )

    qry = qry.select_related('person', 'group')

    # Hide inactive users except for owners and moderators.
    if active is False:
        qry = qry.filter(active=False)
    elif user is None or not user.is_staff:
        qry = qry.filter(Q(active=True) | Q(private_key=voter.private_key))

    # Apply filters.
    if person is not None:
        qry = qry.filter(person=person)
    if group is not None:
        qry = qry.filter(group=group)
    if tag is not None:
        qry = qry.filter(tags__contains=[tag])

    return prep_quotes_for_template(qry[:10])


def query_voter_quotes(voter: Voter) -> List[dict]:
    qry = (
        Quote.objects.
        annotate(
            voter_reaction=FilteredRelation('reactions', condition=Q(
                reactions__private_key=voter.private_key,
            ))
        ).
        filter(Q(active=True) | Q(private_key=voter.private_key)).
        filter(voter_reaction__isnull=False).
        select_related('voter_reaction', 'person').
        order_by('-voter_reaction__created')
    )
    return prep_quotes_for_template(qry[:10], add_voter_reaction)


def add_voter_reaction(quote: Quote, data: dict) -> dict:
    data['reaction'] = quote.voter_reaction.reaction
    return data


def noop(quote: Quote, data: dict) -> dict:
    return data


def prep_quotes_for_template(quotes, prep: Callable = noop) -> List[dict]:
    result = []
    for quote in quotes:
        data = {
            'id': quote.pk,
            'source': {
                'url': quote.source,
                'name': get_source_name(quote.source),
            },
            'text': quote.text,
            'date': quote.date.isoformat(),
            'tags': quote.tags,
            'reaction': None,
            'active': quote.active,
        }
        if quote.person:
            data['group'] = None
            data['person'] = {
                'name': quote.person.name,
                'slug': quote.person.slug,
            }
        else:
            data['person'] = ''
            data['group'] = {
                'name': quote.group.name,
                'slug': quote.group.slug,
            }
        data = prep(quote, data)
        result.append(data)
    return result


class PartyDetails(TypedDict):
    slug: str
    abbr: str
    name: str


class CandidateDetails(TypedDict):
    slug: str
    name: str
    party: Optional[PartyDetails]
    rating: float


def query_voter_candidates(election: Election, voter: Voter) -> List[CandidateDetails]:
    # Prefetch candidate and groups.
    # TODO: FilteredRelation does not work with nested relations:
    #       https://code.djangoproject.com/ticket/29789
    candidates = {
        pk: PartyDetails(
            slug=slug,
            abbr=abbr,
            name=name,
        )
        for pk, slug, abbr, name in (
            Candidate.objects.
            filter(
                group__isnull=False,
                person__isnull=False,
                election=election,
            ).
            only(
                'person__id',
                'group__slug',
                'group__abbr',
                'group__name',
            ).
            values_list(
                'person__id',
                'group__slug',
                'group__abbr',
                'group__name',
            )
        )
    }

    qry = (
        Reaction.objects.
        filter(private_key=voter.private_key).
        # Include only quotes from given election candidates.
        filter(Exists(
            Candidate.objects.filter(
                person__isnull=False,
                person=OuterRef('quote__person'),
                election=election,
            )
        )).
        # TODO: FilteredRelation does not work with nested relations:
        #       https://code.djangoproject.com/ticket/29789
        # annotate(
        #     candidate=FilteredRelation('quote__person__candidate', condition=Q(
        #         person=OuterRef('quote__person'),
        #         election=election,
        #     ))
        # ).
        values(
            'quote__person__id',
            'quote__person__slug',
            'quote__person__name',
        ).
        annotate(rating=Cast(
            (
                Cast(Sum('reaction'), FloatField()) /
                Cast(Count('reaction'), FloatField())
            ),
            FloatField(),
        ) * 100).
        order_by('-rating')
    )
    return [
        CandidateDetails(
            slug=reaction['quote__person__slug'],
            name=reaction['quote__person__name'],
            party=candidates.get(reaction['quote__person__id']),
            rating=int(reaction['rating']),
        )
        for reaction in qry[:100]
    ]

