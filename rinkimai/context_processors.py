import dataclasses

from django.http import HttpRequest
from django.urls import reverse

from rinkimai.services import get_current_election


@dataclasses.dataclass()
class MenuItem:
    name: str
    url: str
    title: str
    staff: bool = False



def menu(request: HttpRequest):
    election = get_current_election()
    return {
        'election': election,
        'menu': [
            MenuItem('home', reverse('home'), 'Pradžia'),
            MenuItem('my-reactions', reverse('my-reactions'), 'Mano reakcijos'),
            MenuItem('my-candidates', reverse('my-candidates'), 'Mano kandidatai'),
            MenuItem('unapproved-quotes', reverse('unapproved-quotes'), 'Nepatvirtinti', staff=True),
        ]
    }
