from django.contrib.auth.models import User
from django.db import models
from django.contrib.postgres.fields import ArrayField
from django.urls import reverse
from django.utils.translation import gettext_lazy as _


class Group(models.Model):
    slug = models.SlugField(unique=True)
    name = models.CharField(max_length=255)
    abbr = models.CharField(max_length=16)
    aliases = ArrayField(models.CharField(max_length=255), blank=True, default=list)

    def __str__(self):
        return self.abbr or self.name

    def get_absolute_url(self):
        return reverse('group-quotes', args=[self.slug])


class Person(models.Model):
    slug = models.SlugField(unique=True)
    name = models.CharField(max_length=255)
    bday = models.DateField(null=True, blank=True)
    image = models.ImageField(null=True, blank=True)
    aliases = ArrayField(models.CharField(max_length=255), blank=True, default=list)

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('person-quotes', args=[self.slug])


class Election(models.Model):

    class Kind(models.TextChoices):
        PRESIDENT = 'president', _("President")
        PARLIAMENT = 'parliament', _("Parliament")

    date = models.DateField(null=True)
    kind = models.CharField(max_length=40, choices=Kind.choices, null=True)

    def __str__(self):
        return self.date.isoformat() + f' ({self.kind})'


class Candidate(models.Model):

    class Kind(models.TextChoices):
        VIENMANDATE = 'vienmandate', _("Vienmandatė")
        DAUGIAMANDATE = 'daugiamandate', _("Daugiamandatė")

    class Meta:
        unique_together = [
            ['election', 'person', 'group'],
        ]

    election = models.ForeignKey(Election, on_delete=models.CASCADE)
    kind = models.CharField(max_length=40, choices=Kind.choices, null=True, blank=False)
    person = models.ForeignKey(Person, on_delete=models.CASCADE, null=True, blank=True)
    group = models.ForeignKey(Group, on_delete=models.CASCADE, null=True, blank=True)
    number = models.PositiveSmallIntegerField(null=True, blank=True)
    name = models.CharField(max_length=255)
    image = models.ImageField(null=True, blank=True)

    def __str__(self):
        return self.name


class Quote(models.Model):
    private_key = models.CharField(max_length=40)
    quote = models.ForeignKey('self', related_name='parent', null=True, blank=True, on_delete=models.CASCADE)
    group = models.ForeignKey(Group, null=True, blank=True, on_delete=models.CASCADE)
    person = models.ForeignKey(Person, null=True, blank=True, on_delete=models.CASCADE)
    source = models.URLField()
    date = models.DateField(null=True)
    text = models.TextField()
    image = models.ImageField(null=True, blank=True)
    tags = ArrayField(models.CharField(max_length=40), blank=True, default=list)
    reaction = models.SmallIntegerField(null=True, blank=True)
    active = models.BooleanField(default=False, blank=True)
    created = models.DateTimeField(auto_now_add=True)
    approved_by = models.ForeignKey(User, null=True, blank=True, default=None, on_delete=models.SET_NULL)

    def __str__(self):
        return self.text

    def get_absolute_url(self):
        return reverse('quote-details', args=[str(self.pk)])


class Reaction(models.Model):
    private_key = models.CharField(max_length=40)
    public_key = models.CharField(max_length=40)
    quote = models.ForeignKey(Quote, related_name='reactions', on_delete=models.CASCADE)
    reaction = models.SmallIntegerField(default=0, blank=True)
    tags = ArrayField(models.CharField(max_length=40), blank=True, default=list)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        unique_together = [
            ['private_key', 'quote'],
        ]


class Moderator(models.Model):
    name = models.CharField(max_length=255)
    public_key = models.CharField(max_length=40)
    active = models.BooleanField(default=False, blank=True)

    def __str__(self):
        return f"{self.name} ({self.public_key})"


class Settings(models.Model):
    election = models.OneToOneField(Election, on_delete=models.CASCADE, null=True, blank=True)
