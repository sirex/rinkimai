from typing import List
from typing import TypedDict
from urllib.parse import urlparse

from django.db.models import Q

from rinkimai.models import Group
from rinkimai.models import Person
from rinkimai.sources import delfi
from rinkimai.sources import lrt
from rinkimai.sources import penkiolikamin


class SuggestedPerson(TypedDict):
    id: str
    label: str


def suggested_person_or_group_for_quote(names: List[str]) -> List[SuggestedPerson]:
    result = []
    for name in names:
        qry = (
            Person.objects.
            only('pk', 'name', 'bday').
            filter(Q(name=name) | Q(aliases__contains=[name]))
        )
        found: List[Person] = list(qry)
        if len(found) == 1:
            person = found[0]
            result.append({
                'id': str(person.pk),
                'label': person.name,
                'type': 'person',
            })
            continue
        elif len(found) > 1:
            for person in found:
                result.append({
                    'id': str(person.pk),
                    'label': f"{person.name} ({person.bday.year})",
                    'type': 'person',
                })
                continue

        qry = (
            Group.objects.
            only('pk', 'name', 'abbr').
            filter(
                Q(name=name) |
                Q(abbr=name) |
                Q(aliases__contains=[name])
            )
        )
        found: List[Group] = list(qry)
        if found:
            for group in found:
                result.append({
                    'id': str(group.pk),
                    'label': group.name,
                    'type': 'group',
                })
            continue

        result.append({
            'id': name,
            'label': name,
            'type': 'person',
        })

    return result


def extract_source_data(source: str):
    if source.startswith('https://www.delfi.lt/'):
        return delfi.parse(source)
    elif source.startswith('https://www.15min.lt/'):
        return penkiolikamin.parse(source)
    elif source.startswith('https://www.lrt.lt/'):
        return lrt.parse(source)
    elif source.startswith('https://tslkd2020.lt/'):
        return {
            'date': '2020-06-22',
            'names': [
                'Tėvynės sąjunga - Lietuvos krikščionys demokratai',
            ],
        }
    elif source.startswith('https://darbopartija.lt/'):
        return {
            'date': '2020-06-28',
            'names': [
                'Darbo partija',
            ],
        }
    else:
        return {
            'date': '',
            'names': [],
        }


def get_source_name(url: str) -> str:
    purl = urlparse(url)
    name = purl.hostname
    if name.startswith('www.'):
        name = name[len('www.'):]
    return name
