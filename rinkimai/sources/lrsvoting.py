from urllib.parse import urlparse
from urllib.parse import parse_qsl

import lxml.etree
import requests

from rinkimai.utils.dates import normalize_date

VOTE_VALUES = {
    'už': 1,
    'susilaikė': 0,
    '': 0,
    'prieš': -1,
}


def parse(url: str):
    purl = urlparse(url)
    query = dict(parse_qsl(purl.query))
    p_bals_id = query['p_bals_id']
    url = f'https://apps.lrs.lt/sip/p2b.ad_sp_balsavimo_rezultatai?balsavimo_id={p_bals_id}'
    resp = requests.get(url)
    xml = lxml.etree.fromstring(resp.content)
    date = xml.xpath('//BendriBalsavimoRezultatai/@balsavimo_laikas')[0]
    reactions = []
    for el in xml.xpath('//IndividualusBalsavimoRezultatas'):
        first_name = el.attrib['vardas']
        last_name = el.attrib['pavardė']
        quote = el.attrib['kaip_balsavo']
        reaction = VOTE_VALUES[quote.lower()]
        name = f"{first_name} {last_name}"
        if reaction:
            reactions.append({
                'date': None,
                'quote': quote,
                'name': name,
                'reaction': reaction,
            })
    return {
        'date': normalize_date(date),
        'quote': '',
        'names': [],
        'reactions': reactions,
    }
