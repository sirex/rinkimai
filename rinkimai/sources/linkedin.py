import urllib.parse

import lxml.html
import requests

from rinkimai.utils.dates import normalize_date


def parse(url: str):
    urlp = urllib.parse.urlparse(url)
    if urlp.query:
        query = dict(urllib.parse.parse_qsl(urlp.query))
    else:
        query = {}
    if 'commentUrn' in query:
        urn = query['commentUrn']
    else:
        return
    resp = requests.get(url)
    resp.raise_for_status()
    html = lxml.html.fromstring(resp.text)
    el = html.xpath(f'//article[data-id="{urn}"]')
    if len(el) > 0:
        el = el[0]
    else:
        return

    name = el.cssselect('.comments-post-meta__name')[0].text_content()
    date = el.cssselect('.comments-comment-item__timestamp')[0].text_content()
    text = el.cssselect('.comments-comment-item__main-content')[0].text_content()
    return {
        'date': normalize_date(date),
        'names': [name],
        'text': text,
    }
