import lxml.html
import requests

from rinkimai.utils.dates import normalize_date
from rinkimai.utils.names import find_people_names
from rinkimai.utils.names import iter_all_people_names
from rinkimai.utils.names import prepare_name_list


def parse(url: str):
    resp = requests.get(url)
    html = lxml.html.fromstring(resp.content)
    date = html.cssselect('article.article-block .published span')[0].text
    article = html.cssselect('article.article-block')[0].text_content()
    names = iter_all_people_names()
    people = prepare_name_list(names)
    return {
        'date': normalize_date(date),
        'names': find_people_names(people, article)
    }
