import lxml.html
import requests

from rinkimai.utils.dates import normalize_date
from rinkimai.utils.names import find_people_names
from rinkimai.utils.names import iter_all_people_names
from rinkimai.utils.names import prepare_name_list


def firstof(*items):
    for item in items:
        if item:
            return item


def parse(url: str):
    resp = requests.get(url)
    html = lxml.html.fromstring(resp.text)
    date = firstof(
        html.cssselect('div.source-date'),
        html.cssselect('div.delfi-source-date'),
    )[0].text
    article = html.cssselect('div.article-body')[0].text_content()
    names = iter_all_people_names()
    people = prepare_name_list(names)
    return {
        'date': normalize_date(date),
        'names': find_people_names(people, article)
    }
